//-------------------------------------------------------------------------
//    Ball.sv                                                            --
//    Viral Mehta                                                        --
//    Spring 2005                                                        --
//                                                                       --
//    Modified by Stephen Kempf 03-01-2006                               --
//                              03-12-2007                               --
//    Translated by Joe Meng    07-07-2013                               --
//    Fall 2014 Distribution                                             --
//                                                                       --
//    For use with ECE 298 Lab 7                                         --
//    UIUC ECE Department                                                --
//-------------------------------------------------------------------------


module ball (input Reset, frame_clk,
				input [7:0] keycode,
				output [9:0] BallX, BallY, BallS, BallL,
				
				output [9:0] CarX, CarY, CarS,
				output [9:0] Car1X, Car1Y, Car1S,
				output [9:0] Car2X, Car2Y, Car2S,
				output [9:0] Car3X, Car3Y, Car3S,
				output [9:0] Car4X, Car4Y, Car4S,
				output [9:0] Car5X, Car5Y, Car5S,
				
				output [9:0] PadX, PadY, PadS,
				output [9:0] Pad1X, Pad1Y, Pad1S,
				output [9:0] Pad2X, Pad2Y, Pad2S,
				
				output [9:0] LifeX, LifeY, LifeS,
				output [9:0] Life1X, Life1Y, Life1S,
				output [9:0] Life2X, Life2Y, Life2S,
				output [9:0] Life3X, Life3Y, Life3S,
				output [9:0] Life4X, Life4Y, Life4S,
				output [9:0] Life5X, Life5Y, Life5S,
				output [9:0] Life6X, Life6Y, Life6S,
				output [9:0] Life7X, Life7Y, Life7S,
				
				output [1:0] slot1,
				output [1:0] slot2,
				output [1:0] slot3,
				output [1:0] slot4,
				output [1:0] slot5,
				
				output [1:0] titlecard);
    
		logic [9:0] Ball_X_Pos, Ball_X_Motion, Ball_Y_Pos, Ball_Y_Motion, Ball_Size, Ball_Life;
		
		logic [9:0] Car_X_Pos, Car_X_Motion, Car_Y_Pos, Car_Y_Motion, Car_Size;
		logic [9:0] Car1_X_Pos, Car1_X_Motion, Car1_Y_Pos, Car1_Y_Motion, Car1_Size;
		logic [9:0] Car2_X_Pos, Car2_X_Motion, Car2_Y_Pos, Car2_Y_Motion, Car2_Size;
		logic [9:0] Car3_X_Pos, Car3_X_Motion, Car3_Y_Pos, Car3_Y_Motion, Car3_Size;
		logic [9:0] Car4_X_Pos, Car4_X_Motion, Car4_Y_Pos, Car4_Y_Motion, Car4_Size;
		logic [9:0] Car5_X_Pos, Car5_X_Motion, Car5_Y_Pos, Car5_Y_Motion, Car5_Size;
		
		logic [9:0] Pad_X_Pos, Pad_X_Motion, Pad_Y_Pos, Pad_Y_Motion, Pad_Size;
		logic [9:0] Pad1_X_Pos, Pad1_X_Motion, Pad1_Y_Pos, Pad1_Y_Motion, Pad1_Size;
		logic [9:0] Pad2_X_Pos, Pad2_X_Motion, Pad2_Y_Pos, Pad2_Y_Motion, Pad2_Size;
		
		logic [9:0] Life_X_Pos, Life_Y_Pos, Life_Size;
		logic [9:0] Life1_X_Pos, Life1_Y_Pos, Life1_Size;
		logic [9:0] Life2_X_Pos, Life2_Y_Pos, Life2_Size;
		logic [9:0] Life3_X_Pos, Life3_Y_Pos, Life3_Size;
		logic [9:0] Life4_X_Pos, Life4_Y_Pos, Life4_Size;
		logic [9:0] Life5_X_Pos, Life5_Y_Pos, Life5_Size;
		logic [9:0] Life6_X_Pos, Life6_Y_Pos, Life6_Size;
		logic [9:0] Life7_X_Pos, Life7_Y_Pos, Life7_Size;
	 
		parameter [9:0] Ball_X_Center=320;  // Center position on the X axis
		parameter [9:0] Ball_Y_Center=240;  // Center position on the Y axis
		parameter [9:0] Ball_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Ball_X_Max=639;     // Rightmost point on the X axis
		var 		 [9:0] Ball_Y_Min=91;      // Topmost point on the Y axis
		parameter [9:0] Ball_Y_Max=463;     // Bottommost point on the Y axis
		parameter [9:0] Ball_X_Step=2;      // Step size on the X axis
		parameter [9:0] Ball_Y_Step=2;      // Step size on the Y axis
	 
		parameter [9:0] Car_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Car_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Car_X_Step=-3;      // Step size on the X axis
	 
		parameter [9:0] Car1_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Car1_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Car1_X_Step=-1;      // Step size on the X axis
		
		parameter [9:0] Car2_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Car2_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Car2_X_Step=-3;      // Step size on the X axis
		
		parameter [9:0] Car3_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Car3_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Car3_X_Step=-1;      // Step size on the X axis
		
		parameter [9:0] Car4_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Car4_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Car4_X_Step=-2;      // Step size on the X axis
		
		parameter [9:0] Car5_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Car5_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Car5_X_Step=-2;      // Step size on the X axis
		
		parameter [9:0] Pad_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Pad_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Pad_X_Step=1;      // Step size on the X axis
		
		parameter [9:0] Pad1_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Pad1_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Pad1_X_Step=1;      // Step size on the X axis
		
		parameter [9:0] Pad2_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Pad2_X_Max=639;     // Rightmost point on the X axis
		parameter [9:0] Pad2_X_Step=1;      // Step size on the X axis
		
		parameter [9:0] Life_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life_X_Max=639;     // Rightmost point on the X axis
		
		parameter [9:0] Life1_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life1_X_Max=639;     // Rightmost point on the X axis
		
		parameter [9:0] Life2_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life2_X_Max=639;     // Rightmost point on the X axis
		
		parameter [9:0] Life3_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life3_X_Max=639;     // Rightmost point on the X axis
		
		parameter [9:0] Life4_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life4_X_Max=639;     // Rightmost point on the X axis
		
		parameter [9:0] Life5_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life5_X_Max=639;     // Rightmost point on the X axis
		
		parameter [9:0] Life6_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life6_X_Max=639;     // Rightmost point on the X axis
		
		parameter [9:0] Life7_X_Min=0;       // Leftmost point on the X axis
		parameter [9:0] Life7_X_Max=639;     // Rightmost point on the X axis

		assign Ball_Size = 8;  // assigns the value 4 as a 10-digit binary number, ie "0000000100"
    
		assign Car_Size = 12;
		
		assign Car1_Size = 12;
		
		assign Car2_Size = 12;
		
		assign Car3_Size = 12;
		
		assign Car4_Size = 12;
		
		assign Car5_Size = 12;
		
		assign Pad_Size = 30;
		
		assign Pad1_Size = 30;
		
		assign Pad2_Size = 13;
		
		assign Life_Size = 4;
		
		assign Life1_Size = 4;
		
		assign Life2_Size = 4;
		
		assign Life3_Size = 4;
		
		assign Life4_Size = 4;
		
		assign Life5_Size = 4;
		
		assign Life6_Size = 4;
		
		assign Life7_Size = 4;
    
		always_ff @ (posedge Reset or posedge frame_clk )
		begin: Move_Ball
			if (Reset)  // Asynchronous Reset
				begin 
				Ball_Y_Motion <= 10'd0; //Ball_Y_Step;
				Ball_X_Motion <= 10'd0; //Ball_X_Step;
				Ball_Y_Pos <= 422;
				Ball_X_Pos <= Ball_X_Center;
				Ball_Life <= 0;
				
				Car_X_Motion <= Car_X_Step;
				Car_Y_Pos <= 394;
				Car_X_Pos <= 624;
				
				slot1 <= 0;
				slot2 <= 0;
				slot3 <= 0;
				slot4 <= 0;
				slot5 <= 0;
				
				titlecard <= 1;
				
				Car1_X_Motion <= Car1_X_Step;
				Car1_Y_Pos <= 354;
				Car1_X_Pos <= 624;
				
				Car2_X_Motion <= Car2_X_Step;
				Car2_Y_Pos <= 394;
				Car2_X_Pos <= 190;
				
				Car3_X_Motion <= Car3_X_Step;
				Car3_Y_Pos <= 354;
				Car3_X_Pos <= 320;
				
				Car4_X_Motion <= Car4_X_Step;
				Car4_Y_Pos <= 315;
				Car4_X_Pos <= 320;
				
				Car5_X_Motion <= Car5_X_Step;
				Car5_Y_Pos <= 315;
				Car5_X_Pos <= 624;
				
				Pad_X_Motion <= Pad_X_Step;
				Pad_Y_Pos <= 243;
				Pad_X_Pos <= Ball_X_Center;
				
				Pad1_X_Motion <= Pad1_X_Step;
				Pad1_Y_Pos <= 185;
				Pad1_X_Pos <= (Ball_X_Center - 150);
				
				Pad2_X_Motion <= Pad2_X_Step;
				Pad2_Y_Pos <= 144;
				Pad2_X_Pos <= (Ball_X_Center + 150);
				
				Life_X_Pos <= 5;
				Life_Y_Pos <= 470;
				
				Life1_X_Pos <= 18;
				Life1_Y_Pos <= 470;
				
				Life2_X_Pos <= 31;
				Life2_Y_Pos <= 470;
				
				Life3_X_Pos <= 44;
				Life3_Y_Pos <= 470;
				
				Life4_X_Pos <= 57;
				Life4_Y_Pos <= 470;
				
				Life5_X_Pos <= 70;
				Life5_Y_Pos <= 470;
				
				Life6_X_Pos <= 83;
				Life6_Y_Pos <= 470;
				
				Life7_X_Pos <= 96;
				Life7_Y_Pos <= 470;
				
				end
           
			else 
				begin 
						
					begin
					Ball_Y_Motion <= 0;
					Ball_X_Motion <= 0;
					
					Car_X_Motion <= Car_X_Step;
					
					Car1_X_Motion <= Car1_X_Step;
					
					Car2_X_Motion <= Car2_X_Step;
					
					Car3_X_Motion <= Car3_X_Step;
					
					Car4_X_Motion <= Car4_X_Step;
					
					Car5_X_Motion <= Car5_X_Step;
					
					Pad_X_Motion <= Pad_X_Step;
					
					Pad1_X_Motion <= Pad1_X_Step;
					
					end
					
					
					Ball_Y_Min <= 91;
					
					if ( (Ball_X_Pos + Ball_Size) <= 81 )
						Ball_Y_Min <= 131;
				 
					if ( (Ball_X_Pos + Ball_Size) >= 111 && (Ball_X_Pos + Ball_Size) <= 192 )
						Ball_Y_Min <= 131;
				 
					if ( (Ball_X_Pos + Ball_Size) >= 223 && (Ball_X_Pos + Ball_Size) <= 304 )
						Ball_Y_Min <= 131;
				 
					if ( (Ball_X_Pos + Ball_Size) >= 336 && (Ball_X_Pos + Ball_Size) <= 417 )
						Ball_Y_Min <= 131;
				 
					if ( (Ball_X_Pos + Ball_Size) >= 447 && (Ball_X_Pos + Ball_Size) <= 528 )
						Ball_Y_Min <= 131;
				 
					if ( (Ball_X_Pos + Ball_Size) >= 558 && (Ball_X_Pos + Ball_Size) <= 639 )
						Ball_Y_Min <= 131;
				 
				 
					case (keycode)
						8'h04 : begin
							Ball_X_Motion <= -2;//A
							Ball_Y_Motion<= 0;
							end
					        
						8'h07 : begin
							Ball_X_Motion <= 2;//D
							Ball_Y_Motion <= 0;
							end
  
						8'h16 : begin
							Ball_Y_Motion <= 2;//S
							Ball_X_Motion <= 0;
							end
							  
						8'h1A : begin
					      Ball_Y_Motion <= -2;//W
							Ball_X_Motion <= 0;
							end
						8'h28 :begin
							titlecard <= 0;//enter
							end

						default: ;
					endcase
					
				 
					if ( (Ball_Y_Pos + Ball_Size) >= Ball_Y_Max )  // Ball is at the bottom edge, BOUNCE!
						begin
						Ball_Y_Pos <= Ball_Y_Min;
						Ball_Y_Motion <= (~ (Ball_Y_Step) + 1'b1);
						end
					  
					else if ( (Ball_Y_Pos - Ball_Size) <= Ball_Y_Min )  // Ball is at the top edge, BOUNCE!
						begin
						Ball_Y_Pos <= Ball_Y_Min;
						Ball_Y_Motion <= Ball_Y_Step;
						end
					  
					else if ( (Ball_X_Pos + Ball_Size) >= Ball_X_Max )  // Ball is at the Right edge, BOUNCE!
						begin
						Ball_X_Pos <= Ball_X_Max;
						Ball_X_Motion <= (~ (Ball_X_Step) + 1'b1);
						end
					  
					else if ( (Ball_X_Pos - Ball_Size) <= Ball_X_Min )  // Ball is at the Left edge, BOUNCE!
						begin
						Ball_X_Pos <= Ball_X_Min;
						Ball_X_Motion <= Ball_X_Step;
						end
					
					if ( (Pad_X_Pos + Pad_Size) >= Pad_X_Max )  // Ball is at the Right edge, BOUNCE!
						begin
						Pad_X_Motion <= (~ (Pad_X_Step) + 1'b1);  // 2's complement.
						end
					else if ( (Pad_X_Pos - (Pad_Size + 1)) <= Pad_X_Min )  // Ball is at the Left edge, BOUNCE!
						begin
						Pad_X_Motion <= Pad_X_Step;
						end
					else 
						begin
						Pad_X_Motion <= Pad_X_Motion;  // Ball is somewhere in the middle, don't bounce
						end
					
					Pad_X_Pos <= (Pad_X_Pos + Pad_X_Motion);
					
					if ( (Pad1_X_Pos + Pad1_Size) >= Pad1_X_Max )  // Ball is at the Right edge, BOUNCE!
						begin
						Pad1_X_Motion <= (~ (Pad1_X_Step) + 1'b1);  // 2's complement.
						end
					else if ( (Pad1_X_Pos - (Pad1_Size + 1)) <= Pad1_X_Min )  // Ball is at the Left edge, BOUNCE!
						begin
						Pad1_X_Motion <= Pad1_X_Step;
						end
					else 
						begin
						Pad1_X_Motion <= Pad1_X_Motion;  // Ball is somewhere in the middle, don't bounce
						end
					
					Pad1_X_Pos <= (Pad1_X_Pos + Pad1_X_Motion);
					
					if ( (Pad2_X_Pos + Pad2_Size) >= Pad2_X_Max )  // Ball is at the Right edge, BOUNCE!
						begin
						Pad2_X_Motion <= (~ (Pad2_X_Step) + 1'b1);  // 2's complement.
						end
					else if ( (Pad2_X_Pos - (Pad2_Size + 1)) <= Pad1_X_Min )  // Ball is at the Left edge, BOUNCE!
						begin
						Pad2_X_Motion <= Pad2_X_Step;
						end
					else 
						begin
						Pad2_X_Motion <= Pad2_X_Motion;  // Ball is somewhere in the middle, don't bounce
						end
					
					Pad2_X_Pos <= (Pad2_X_Pos + Pad2_X_Motion);
			
					if ( (Ball_X_Pos + Ball_Size) >= 81 && (Ball_X_Pos + Ball_Size) <= 111 && (Ball_Y_Pos + Ball_Size) <= 125 )
						begin
						Ball_Life <= Ball_Life + 1;
						slot1 <= 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= 192 && (Ball_X_Pos + Ball_Size) <= 223 && (Ball_Y_Pos + Ball_Size) <= 125 )
						begin
						Ball_Life <= Ball_Life + 1;
						slot2 <= 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= 304 && (Ball_X_Pos + Ball_Size) <= 336 && (Ball_Y_Pos + Ball_Size) <= 125 )
						begin
						Ball_Life <= Ball_Life + 1;
						slot3 <= 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= 417 && (Ball_X_Pos + Ball_Size) <= 447 && (Ball_Y_Pos + Ball_Size) <= 125 )
						begin
						Ball_Life <= Ball_Life + 1;
						slot4 <= 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= 528 && (Ball_X_Pos + Ball_Size) <= 558 && (Ball_Y_Pos + Ball_Size) <= 125 )
						begin
						Ball_Life <= Ball_Life + 1;
						slot5 <= 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= (Car_X_Pos - Car_Size) && (Ball_X_Pos - Ball_Size) <= (Car_X_Pos + Car_Size) && (Ball_Y_Pos - Ball_Size) >= (Car_Y_Pos - Car_Size) && (Ball_Y_Pos + Ball_Size) <= (Car_Y_Pos + Car_Size))
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= (Car1_X_Pos - Car1_Size) && (Ball_X_Pos - Ball_Size) <= (Car1_X_Pos + Car1_Size) && (Ball_Y_Pos - Ball_Size) >= (Car1_Y_Pos - Car1_Size) && (Ball_Y_Pos + Ball_Size) <= (Car1_Y_Pos + Car1_Size))
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= (Car2_X_Pos - Car2_Size) && (Ball_X_Pos - Ball_Size) <= (Car2_X_Pos + Car2_Size) && (Ball_Y_Pos - Ball_Size) >= (Car2_Y_Pos - Car2_Size) && (Ball_Y_Pos + Ball_Size) <= (Car2_Y_Pos + Car2_Size))
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= (Car3_X_Pos - Car3_Size) && (Ball_X_Pos - Ball_Size) <= (Car3_X_Pos + Car3_Size) && (Ball_Y_Pos - Ball_Size) >= (Car3_Y_Pos - Car3_Size) && (Ball_Y_Pos + Ball_Size) <= (Car3_Y_Pos + Car3_Size))
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= (Car4_X_Pos - Car4_Size) && (Ball_X_Pos - Ball_Size) <= (Car4_X_Pos + Car4_Size) && (Ball_Y_Pos - Ball_Size) >= (Car4_Y_Pos - Car4_Size) && (Ball_Y_Pos + Ball_Size) <= (Car4_Y_Pos + Car4_Size))
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_X_Pos + Ball_Size) >= (Car5_X_Pos - Car5_Size) && (Ball_X_Pos - Ball_Size) <= (Car5_X_Pos + Car5_Size) && (Ball_Y_Pos - Ball_Size) >= (Car5_Y_Pos - Car5_Size) && (Ball_Y_Pos + Ball_Size) <= (Car5_Y_Pos + Car5_Size))
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_Y_Pos + Ball_Size) <= 271 && (Ball_Y_Pos - Ball_Size) >= 213 )
						if ((Ball_X_Pos + Ball_Size) >= (Pad_X_Pos - Pad_Size) && (Ball_X_Pos - Ball_Size) <= (Pad_X_Pos + Pad_Size) && (Ball_Y_Pos - Ball_Size) >= (Pad_Y_Pos - Pad_Size) && (Ball_Y_Pos + Ball_Size) <= (Pad_Y_Pos + Pad_Size))
						begin
						Ball_Y_Pos <= (Ball_Y_Pos + Ball_Y_Motion);
						Ball_X_Pos <= (Ball_X_Pos + Pad_X_Motion);
						end
						else
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_Y_Pos + Ball_Size) <= 213 && (Ball_Y_Pos - Ball_Size) >= 155 )
						if ((Ball_X_Pos + Ball_Size) >= (Pad1_X_Pos - Pad1_Size) && (Ball_X_Pos - Ball_Size) <= (Pad1_X_Pos + Pad1_Size) && (Ball_Y_Pos - Ball_Size) >= (Pad1_Y_Pos - Pad1_Size) && (Ball_Y_Pos + Ball_Size) <= (Pad1_Y_Pos + Pad1_Size))
						begin
						Ball_Y_Pos <= (Ball_Y_Pos + Ball_Y_Motion);
						Ball_X_Pos <= (Ball_X_Pos + Pad1_X_Motion);
						end
						else
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else if ( (Ball_Y_Pos + Ball_Size) <= 155 && (Ball_Y_Pos - Ball_Size) >= 131 )
						if ((Ball_X_Pos + Ball_Size) >= (Pad2_X_Pos - Pad2_Size) && (Ball_X_Pos - Ball_Size) <= (Pad2_X_Pos + Pad2_Size) && (Ball_Y_Pos - Ball_Size) >= (Pad2_Y_Pos - Pad2_Size) && (Ball_Y_Pos + Ball_Size) <= (Pad2_Y_Pos + Pad2_Size))
						begin
						Ball_Y_Pos <= (Ball_Y_Pos + Ball_Y_Motion);
						Ball_X_Pos <= (Ball_X_Pos + Pad2_X_Motion);
						end
						else
						begin
						Ball_Life <= Ball_Life + 1;
						Ball_Y_Pos <= 422;
						Ball_X_Pos <= Ball_X_Center;
						Ball_X_Motion <= 0;
						Ball_Y_Motion <= 0;
						end
					else
						begin
						Ball_Y_Pos <= (Ball_Y_Pos + Ball_Y_Motion);  // Update ball position
						Ball_X_Pos <= (Ball_X_Pos + Ball_X_Motion);
						end

		
					if ( (Car_X_Pos + Car_Size) <= Car_X_Min )
						begin
						Car_X_Pos <= (Car_X_Max + Car_Size);
						end
					else 
						begin
						Car_X_Pos <= (Car_X_Pos + Car_X_Motion);
						end
					
					if ( (Car1_X_Pos + Car1_Size) <= Car1_X_Min )
						begin
						Car1_X_Pos <= (Car1_X_Max + Car1_Size);
						end
					else 
						begin
						Car1_X_Pos <= (Car1_X_Pos + Car1_X_Motion);
						end
				
					if ( (Car2_X_Pos + Car2_Size) <= Car2_X_Min )
						begin
						Car2_X_Pos <= (Car2_X_Max + Car2_Size);
						end
					else 
						begin
						Car2_X_Pos <= (Car2_X_Pos + Car2_X_Motion);
						end
						
					if ( (Car3_X_Pos + Car3_Size) <= Car3_X_Min )
						begin
						Car3_X_Pos <= (Car3_X_Max + Car3_Size);
						end
					else 
						begin
						Car3_X_Pos <= (Car3_X_Pos + Car3_X_Motion);
						end
						
					if ( (Car4_X_Pos - Car4_Size) <= Car4_X_Min )
						begin
						Car4_X_Pos <= (624);
						end
					else 
						begin
						Car4_X_Pos <= (Car4_X_Pos + Car4_X_Motion);
						end
						
					if ( (Car5_X_Pos - Car5_Size) <= Car5_X_Min )
						begin
						Car5_X_Pos <= (624);
						end
					else 
						begin
						Car5_X_Pos <= (Car5_X_Pos + Car5_X_Motion);
						end
			
			
	  /**************************************************************************************
	    ATTENTION! Please answer the following quesiton in your lab report! Points will be allocated for the answers!
		 Hidden Question #2/2:
          Note that Ball_Y_Motion in the above statement may have been changed at the same clock edge
          that is causing the assignment of Ball_Y_pos.  Will the new value of Ball_Y_Motion be used,
          or the old?  How will this impact behavior of the ball during a bounce, and how might that 
          interact with a response to a keypress?  Can you fix it?  Give an answer in your Post-Lab.
      **************************************************************************************/
      
			
		end  
	end
       
	assign BallX = Ball_X_Pos;
   
   assign BallY = Ball_Y_Pos;
   
   assign BallS = Ball_Size;
	
	assign BallL = Ball_Life;
    
	assign CarX = Car_X_Pos;
	 
	assign CarY = Car_Y_Pos;
	 
	assign CarS = Car_Size;
	
	assign Car1X = Car1_X_Pos;
	 
	assign Car1Y = Car1_Y_Pos;
	 
	assign Car1S = Car1_Size;
	
	assign Car2X = Car2_X_Pos;
	 
	assign Car2Y = Car2_Y_Pos;
	 
	assign Car2S = Car2_Size;

	assign Car3X = Car3_X_Pos;
	 
	assign Car3Y = Car3_Y_Pos;
	 
	assign Car3S = Car3_Size;
	
	assign Car4X = Car4_X_Pos;
	 
	assign Car4Y = Car4_Y_Pos;
	 
	assign Car4S = Car4_Size;
	
	assign Car5X = Car5_X_Pos;
	 
	assign Car5Y = Car5_Y_Pos;
	 
	assign Car5S = Car5_Size;

	assign PadX = Pad_X_Pos;
	 
	assign PadY = Pad_Y_Pos;
	 
	assign PadS = Pad_Size;
	
	assign Pad1X = Pad1_X_Pos;
	 
	assign Pad1Y = Pad1_Y_Pos;
	 
	assign Pad1S = Pad1_Size;

	assign Pad2X = Pad2_X_Pos;
	 
	assign Pad2Y = Pad2_Y_Pos;
	 
	assign Pad2S = Pad2_Size;
	
	assign LifeX = Life_X_Pos;
	 
	assign LifeY = Life_Y_Pos;
	 
	assign LifeS = Life_Size;

	assign Life1X = Life1_X_Pos;
	 
	assign Life1Y = Life1_Y_Pos;
	 
	assign Life1S = Life1_Size;
	
	assign Life2X = Life2_X_Pos;
	 
	assign Life2Y = Life2_Y_Pos;
	 
	assign Life2S = Life2_Size;

	assign Life3X = Life3_X_Pos;
	 
	assign Life3Y = Life3_Y_Pos;
	 
	assign Life3S = Life3_Size;

	assign Life4X = Life4_X_Pos;
	 
	assign Life4Y = Life4_Y_Pos;
	 
	assign Life4S = Life4_Size;

	assign Life5X = Life5_X_Pos;
	 
	assign Life5Y = Life5_Y_Pos;
	 
	assign Life5S = Life5_Size;

	assign Life6X = Life6_X_Pos;
	 
	assign Life6Y = Life6_Y_Pos;
	 
	assign Life6S = Life6_Size;

	assign Life7X = Life7_X_Pos;
	 
	assign Life7Y = Life7_Y_Pos;
	 
	assign Life7S = Life7_Size;

endmodule
