//-------------------------------------------------------------------------
//    Color_Mapper.sv                                                    --
//    Stephen Kempf                                                      --
//    3-1-06                                                             --
//                                                                       --
//    Modified by David Kesler  07-16-2008                               --
//    Translated by Joe Meng    07-07-2013                               --
//                                                                       --
//    Fall 2014 Distribution                                             --
//                                                                       --
//    For use with ECE 385 Lab 7                                         --
//    University of Illinois ECE Department                              --
//-------------------------------------------------------------------------
//    Color_Mapper.sv                                                    --
//    Stephen Kempf                                                      --
//    3-1-06                                                             --
//                                                                       --
//    Modified by David Kesler  07-16-2008                               --
//    Translated by Joe Meng    07-07-2013                               --
//                                                                       --
//    Fall 2014 Distribution                                             --
//                                                                       --
//    For use with ECE 385 Lab 7                                         --
//    University of Illinois ECE Department                              --
//-------------------------------------------------------------------------


module  color_mapper ( input        [9:0] BallX, BallY, Ball_size, BallL, DrawX, DrawY, CarX, CarY, CarS, Car1X, Car1Y, Car1S, Car2X, Car2Y, Car2S, Car3X, Car3Y, Car3S, Car4X, Car4Y, Car4S, Car5X, Car5Y, Car5S, PadX, PadY, PadS, Pad1X, Pad1Y, Pad1S, Pad2X, Pad2Y, Pad2S, 
							  input			[9:0] LifeX, LifeY, LifeS,
							  input			[9:0] Life1X, Life1Y, Life1S,
							  input			[9:0] Life2X, Life2Y, Life2S,
							  input			[9:0] Life3X, Life3Y, Life3S,
							  input			[9:0] Life4X, Life4Y, Life4S,
							  input			[9:0] Life5X, Life5Y, Life5S,
							  input			[9:0] Life6X, Life6Y, Life6S,
							  input			[9:0] Life7X, Life7Y, Life7S,
							  input			[1:0] slot1,
							  input			[1:0] slot2,
							  input			[1:0] slot3,
							  input			[1:0] slot4,
							  input			[1:0] slot5,
							  input			[1:0] titlecard, 
							  input			[8:0] seconds,
							  input     Clk,
                       output logic [7:0] Red, Green, Blue,
							  output	PadOn );
    
    logic ball_on;
	 logic car_on;
	 logic car1_on;
	 logic car2_on;
	 logic car3_on;
	 logic car4_on;
	 logic car5_on;
	 logic pad_on;
	 logic pad1_on;
	 logic pad2_on;
	 logic title_on;
	 logic [18:0] read_address;
	 logic [18:0] read_address1;
	 logic [18:0] read_address2;
	 logic [4:0] TitleRed, TitleGreen, TitleBlue;
	 logic [4:0] TitleCardRed, TitleCardGreen, TitleCardBlue;
	 logic [4:0] WinRed, WinGreen, WinBlue;
	 logic [4:0] LoseRed, LoseGreen, LoseBlue;
	 logic life_on;
	 logic life1_on;
	 logic life2_on;
	 logic life3_on;
	 logic life4_on;
	 logic life5_on;
	 logic life6_on;
	 logic life7_on;
	 logic titlecard_on;
	 logic win_on;
	 logic lose_on;
	 logic countdown_on;
	 
	 int startX, startY, width, height;
	 assign startX = 57;
	 assign startY = 0;
	 assign width = 526;
	 assign height = 61;
	 
	 assign read_address = (DrawX - startX) + ((DrawY - startY)*(width));
	 
	 TitleRed t0(.read_address(read_address), .red_out(TitleRed), .Clk(Clk));

	 TitleGreen t1(.read_address(read_address), .green_out(TitleGreen), .Clk(Clk));

	 TitleBlue t2(.read_address(read_address), .blue_out(TitleBlue), .Clk(Clk));
	 
	 
	 int TCstartX, TCstartY, TCwidth, TCheight;
	 assign TCstartX = 145;
	 assign TCstartY = 190;
	 assign TCwidth = 350;
	 assign TCheight = 100;
	 
	 assign read_address1 = (DrawX - TCstartX) + ((DrawY - TCstartY)*(TCwidth));
	 
	 
	 TitleCardRed t3(.read_address1(read_address1), .red_out1(TitleCardRed), .Clk(Clk));
	 
	 TitleCardGreen t4(.read_address1(read_address1), .green_out1(TitleCardGreen), .Clk(Clk));
	 
	 TitleCardBlue t5(.read_address1(read_address1), .blue_out1(TitleCardBlue), .Clk(Clk));
	 
	 int WINstartX, WINstartY, WINwidth, WINheight;
	 assign WINstartX = 20;
	 assign WINstartY = 165;
	 assign WINwidth = 600;
	 assign WINheight = 150;
	 
/*	 assign read_address2 = (DrawX - WINstartX) + ((DrawY - WINstartY)*(WINwidth));
	 
	 WinRed t6(.read_address2(read_address2), .red_out2(WinRed), .Clk(Clk));
	 
	 WinGreen t7(.read_address2(read_address2), .green_out2(WinGreen), .Clk(Clk));
	 
	 WinBlue t8(.read_address2(read_address2), .blue_out2(WinBlue), .Clk(Clk));
	 
	 LoseRed t9(.read_address2(read_address2), .red_out3(LoseRed), .Clk(Clk));
	 
	 LoseGreen ta(.read_address2(read_address2), .green_out3(LoseGreen), .Clk(Clk));
	 
	 LoseBlue tb(.read_address2(read_address2), .blue_out3(LoseBlue), .Clk(Clk)); */

 /* Old Ball: Generated square box by checking if the current pixel is within a square of length
    2*Ball_Size, centered at (BallX, BallY).  Note that this requires unsigned comparisons.
	 
    if ((DrawX >= BallX - Ball_size) &&
       (DrawX <= BallX + Ball_size) &&
       (DrawY >= BallY - Ball_size) &&
       (DrawY <= BallY + Ball_size))

     New Ball: Generates (pixelated) circle by using the standard circle formula.  Note that while 
     this single line is quite powerful descriptively, it causes the synthesis tool to use up three
     of the 12 available multipliers on the chip!  Since the multiplicants are required to be signed,
	  we have to first cast them from logic to int (signed by default) before they are multiplied). */
	  
    int DistX, DistY, Size;
	 assign DistX = DrawX - BallX;
    assign DistY = DrawY - BallY;
    assign Size = Ball_size;
	 
	 int Car_DistX, Car_DistY, Car_Size;
	 assign Car_DistX = DrawX - CarX;
	 assign Car_DistY = DrawY - CarY;
	 assign Car_Size = CarS;
	 
	 int fill1, fill2, fill3, fill4, fill5, TitleCard;
	 assign fill1 = slot1;
	 assign fill2 = slot2;
	 assign fill3 = slot3;
	 assign fill4 = slot4;
	 assign fill5 = slot5;
	 assign TitleCard = titlecard;
	
	 int Car1_DistX, Car1_DistY, Car1_Size;
	 assign Car1_DistX = DrawX - Car1X;
	 assign Car1_DistY = DrawY - Car1Y;
	 assign Car1_Size = Car1S;
	 
	 int Car2_DistX, Car2_DistY, Car2_Size;
	 assign Car2_DistX = DrawX - Car2X;
	 assign Car2_DistY = DrawY - Car2Y;
	 assign Car2_Size = Car2S;
	 
	 int Car3_DistX, Car3_DistY, Car3_Size;
	 assign Car3_DistX = DrawX - Car3X;
	 assign Car3_DistY = DrawY - Car3Y;
	 assign Car3_Size = Car3S;
	 
	 int Car4_DistX, Car4_DistY, Car4_Size;
	 assign Car4_DistX = DrawX - Car4X;
	 assign Car4_DistY = DrawY - Car4Y;
	 assign Car4_Size = Car4S;
	 
	 int Car5_DistX, Car5_DistY, Car5_Size;
	 assign Car5_DistX = DrawX - Car5X;
	 assign Car5_DistY = DrawY - Car5Y;
	 assign Car5_Size = Car5S;
	 
	 int Pad_DistX, Pad_DistY, Pad_Size;
	 assign Pad_DistX = DrawX - PadX;
	 assign Pad_DistY = DrawY - PadY;
	 assign Pad_Size = PadS;
	 
	 int Pad1_DistX, Pad1_DistY, Pad1_Size;
	 assign Pad1_DistX = DrawX - Pad1X;
	 assign Pad1_DistY = DrawY - Pad1Y;
	 assign Pad1_Size = Pad1S;
	 
	 int Pad2_DistX, Pad2_DistY, Pad2_Size;
	 assign Pad2_DistX = DrawX - Pad2X;
	 assign Pad2_DistY = DrawY - Pad2Y;
	 assign Pad2_Size = Pad2S;
	 
	 int Life_DistX, Life_DistY, Life_Size;
	 assign Life_DistX = DrawX - LifeX;
	 assign Life_DistY = DrawY - LifeY;
	 assign Life_Size = LifeS;
	 
	 int Life1_DistX, Life1_DistY, Life1_Size;
	 assign Life1_DistX = DrawX - Life1X;
	 assign Life1_DistY = DrawY - Life1Y;
	 assign Life1_Size = Life1S;
	 
	 int Life2_DistX, Life2_DistY, Life2_Size;
	 assign Life2_DistX = DrawX - Life2X;
	 assign Life2_DistY = DrawY - Life2Y;
	 assign Life2_Size = Life2S;
	 
	 int Life3_DistX, Life3_DistY, Life3_Size;
	 assign Life3_DistX = DrawX - Life3X;
	 assign Life3_DistY = DrawY - Life3Y;
	 assign Life3_Size = Life3S;
	 
	 int Life4_DistX, Life4_DistY, Life4_Size;
	 assign Life4_DistX = DrawX - Life4X;
	 assign Life4_DistY = DrawY - Life4Y;
	 assign Life4_Size = Life4S;
	 
	 int Life5_DistX, Life5_DistY, Life5_Size;
	 assign Life5_DistX = DrawX - Life5X;
	 assign Life5_DistY = DrawY - Life5Y;
	 assign Life5_Size = Life5S;
	 
	 int Life6_DistX, Life6_DistY, Life6_Size;
	 assign Life6_DistX = DrawX - Life6X;
	 assign Life6_DistY = DrawY - Life6Y;
	 assign Life6_Size = Life6S;
	 
	 int Life7_DistX, Life7_DistY, Life7_Size;
	 assign Life7_DistX = DrawX - Life7X;
	 assign Life7_DistY = DrawY - Life7Y;
	 assign Life7_Size = Life7S;
	 
	 int countX, countY, countW, countH;
	 assign countX = 320;
	 assign countY = 464;
	 assign countW = seconds;
	 assign countH = 16;
	 
    always_comb
    begin:Ball_on_proc
        if ( ( DistX*DistX + DistY*DistY) <= (Size * Size) ) 
            ball_on = 1'b1;
        else 
            ball_on = 1'b0;
 
		  if ( ( Car_DistX*Car_DistX + Car_DistY*Car_DistY) <= (Car_Size * Car_Size) )
				car_on = 1'b1;
		  else
				car_on = 1'b0;
				
		  if ( ( Car1_DistX*Car1_DistX + Car1_DistY*Car1_DistY) <= (Car1_Size * Car1_Size) )
				car1_on = 1'b1;
		  else
				car1_on = 1'b0;
				
		  if ( ( Car2_DistX*Car2_DistX + Car2_DistY*Car2_DistY) <= (Car2_Size * Car2_Size) )
				car2_on = 1'b1;
		  else
				car2_on = 1'b0;
				
		  if ( ( Car3_DistX*Car3_DistX + Car3_DistY*Car3_DistY) <= (Car3_Size * Car3_Size) )
				car3_on = 1'b1;
		  else
				car3_on = 1'b0;
		  
		  if ( ( Car4_DistX*Car4_DistX + Car4_DistY*Car4_DistY) <= (Car4_Size * Car4_Size) )
				car4_on = 1'b1;
		  else
				car4_on = 1'b0;
		  
		  if ( ( Car5_DistX*Car5_DistX + Car5_DistY*Car5_DistY) <= (Car5_Size * Car5_Size) )
				car5_on = 1'b1;
		  else
				car5_on = 1'b0;
		  
		  if ( ( Pad_DistX*Pad_DistX + Pad_DistY*Pad_DistY) <= (Pad_Size * Pad_Size) ) 
            pad_on = 1'b1;
        else 
            pad_on = 1'b0;
		  
		  if ( ( Pad1_DistX*Pad1_DistX + Pad1_DistY*Pad1_DistY) <= (Pad1_Size * Pad1_Size) ) 
            pad1_on = 1'b1;
        else 
            pad1_on = 1'b0;
				
		  if ( ( Pad2_DistX*Pad2_DistX + Pad2_DistY*Pad2_DistY) <= (Pad2_Size * Pad2_Size) ) 
            pad2_on = 1'b1;
        else 
            pad2_on = 1'b0;
		  
		  if ( ( Life_DistX*Life_DistX + Life_DistY*Life_DistY) <= (Life_Size * Life_Size) && BallL <= 7) 
            life_on = 1'b1;
        else 
            life_on = 1'b0;

		  if ( ( Life1_DistX*Life1_DistX + Life1_DistY*Life1_DistY) <= (Life1_Size * Life1_Size) && BallL <= 6) 
            life1_on = 1'b1;
        else 
            life1_on = 1'b0;
				
		  if ( ( Life2_DistX*Life2_DistX + Life2_DistY*Life2_DistY) <= (Life2_Size * Life2_Size) && BallL <= 5) 
            life2_on = 1'b1;
        else 
            life2_on = 1'b0;
		
		  if ( ( Life3_DistX*Life3_DistX + Life3_DistY*Life3_DistY) <= (Life3_Size * Life3_Size) && BallL <= 4) 
            life3_on = 1'b1;
        else 
            life3_on = 1'b0;
				
		  if ( ( Life4_DistX*Life4_DistX + Life4_DistY*Life4_DistY) <= (Life4_Size * Life4_Size) && BallL <= 3) 
            life4_on = 1'b1;
        else 
            life4_on = 1'b0;
				
		  if ( ( Life5_DistX*Life5_DistX + Life5_DistY*Life5_DistY) <= (Life5_Size * Life5_Size) && BallL <= 2) 
            life5_on = 1'b1;
        else 
            life5_on = 1'b0;
				
		  if ( ( Life6_DistX*Life6_DistX + Life6_DistY*Life6_DistY) <= (Life6_Size * Life6_Size) && BallL <= 1) 
            life6_on = 1'b1;
        else 
            life6_on = 1'b0;
		  
		  if ( ( Life7_DistX*Life7_DistX + Life7_DistY*Life7_DistY) <= (Life7_Size * Life7_Size) && BallL <= 0) 
            life7_on = 1'b1;
        else 
            life7_on = 1'b0;
				
		  if (TitleCard == 1)
				titlecard_on = 1'b1;
		  else
				titlecard_on = 1'b0;
		  
		  if ((DrawX >= startX && DrawX <= startX + width && DrawY >= startY && DrawY <= startY + height))
				title_on = 1'b1;
		  else
				title_on = 1'b0;
				
		  if ((DrawX >= countX && DrawX <= countX + countW && DrawY >= countY && DrawY <= countY + countH))
				countdown_on = 1'b1;
		  else
				countdown_on = 1'b0;
		  
		  if ((fill1 == 1 && fill2 == 1 && fill3 == 1 && fill4 == 1 && fill5 == 1))
				win_on = 1'b1;
		  else
				win_on = 1'b0;
				
		  if ((BallL >= 8 && win_on <= 1'b0))
				lose_on = 1'b1;
		  else if ((seconds == 1 && win_on <= 1'b0))
				lose_on = 1'b1;
		  else
				lose_on = 1'b0;
		  
	  end
	  
    always_comb
    begin:RGB_Display
	
	 if (titlecard_on == 1'b1)
		begin
		if ((DrawX >= TCstartX && DrawX <= TCstartX + TCwidth && DrawY >= TCstartY && DrawY <= TCstartY + TCheight))
			begin
			Red = TitleCardGreen;
			Green = TitleCardRed;
			Blue = TitleCardBlue;
			//Red = 8'h00;
			//Green = 8'h00;
			//Blue = 8'h00;
			end
		else
			begin
			Red = 8'h00;
			Green = 8'h00;
			Blue = 8'h00;
			end 
		end
	 else if (win_on == 1'b1)
		begin
		if ((DrawX >= WINstartX && DrawX <= WINstartX + WINwidth && DrawY >= WINstartY && DrawY <= WINstartY + WINheight))
			begin
			//Red = WinRed;
			//Green = WinGreen;
			//Blue = WinBlue;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
			end
		else
			begin
			Red = 8'h00;
			Green = 8'h00;
			Blue = 8'h00;
			end 
		end
	 else if (lose_on == 1'b1)
		begin
		if ((DrawX >= WINstartX && DrawX <= WINstartX + WINwidth && DrawY >= WINstartY && DrawY <= WINstartY + WINheight))
			begin
			//Red = LoseRed;
			//Green = LoseGreen;
			//Blue = LoseBlue;
			Red = 8'hff;
			Green = 8'h00;
			Blue = 8'h00;
			end
		else
			begin
			Red = 8'h00;
			Green = 8'h00;
			Blue = 8'h00;
			end 
		end
		else
		begin
        if ((ball_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end
		  else if ((countdown_on == 1'b1)) 
        begin 
            if ((countW >= 150))
				begin
				Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
				end
				else if ((countW <= 150 && countW >= 75))
				begin
				Red = 8'hfe;
            Green = 8'he6;
            Blue = 8'h16;
				end
				else if ((countW <= 75 && countW >= 1))
				begin
				Red = 8'hff;
            Green = 8'h00;
            Blue = 8'h00;
				end
				else
				begin
				Red = 8'h00;
            Green = 8'h00;
            Blue = 8'h00;
				end
        end 
		  else if ((life_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end
		  else if ((life1_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end       
		  else if ((life2_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end       
		  else if ((life3_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end       
		  else if ((life4_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end       
		  else if ((life5_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end       
		  else if ((life6_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end       
		  else if ((life7_on == 1'b1)) 
        begin 
            Red = 8'h00;
            Green = 8'hff;
            Blue = 8'h00;
        end       
		  else if ((pad_on == 1'b1)) 
        begin 
            Red = 8'h0c;
            Green = 8'h83;
            Blue = 8'h30;
        end       
		  else if ((pad1_on == 1'b1)) 
        begin 
            Red = 8'h0c;
            Green = 8'h83;
            Blue = 8'h30;
        end       
		  else if ((pad2_on == 1'b1)) 
        begin 
            Red = 8'h0c;
            Green = 8'h83;
            Blue = 8'h30;
        end       
        else if ((DrawX>=0 && DrawX<=639 && DrawY>=409 && DrawY<=463))
        begin 
            Red = 8'h6e; 
            Green = 8'h00;
            Blue = 8'h9c;
        end
		  else if ((DrawX>=0 && DrawX<=639 && DrawY>=271 && DrawY<=300))
		  begin	
				Red = 8'h6e;
				Green = 8'h00;
				Blue = 8'h9c;
		  end
		  else if ((DrawX>=0 && DrawX<=639 && DrawY>=131 && DrawY<=271))
		  begin	
				Red = 8'h00;
				Green = 8'h10;
				Blue = 8'h36;
		  end
		  else if ((DrawX>=0 && DrawX<=81 && DrawY>=61 && DrawY<=131))
		  begin	
				Red = 8'h7a;
				Green = 8'hde;
				Blue = 8'h37;
		  end
		  else if ((DrawX>=81 && DrawX<=111 && DrawY>=91 && DrawY<=131))
		  begin
				if (fill1 >= 1)
					begin
					Red = 8'h00;
					Green = 8'hff;
					Blue = 8'h00;
					end
				else
					begin
					Red = 8'h00;
					Green = 8'h10;
					Blue = 8'h36;
					end
		  end
		  else if ((DrawX>=111 && DrawX<=192 && DrawY>=91 && DrawY<=131))
		  begin	
				Red = 8'h7a;
				Green = 8'hde;
				Blue = 8'h37;
		  end
		  else if ((DrawX>=192 && DrawX<=223 && DrawY>=91 && DrawY<=131))
		  begin	
				if (fill2 >= 1)
					begin
					Red = 8'h00;
					Green = 8'hff;
					Blue = 8'h00;
					end
				else
					begin
					Red = 8'h00;
					Green = 8'h10;
					Blue = 8'h36;
					end
		  end
		  else if ((DrawX>=223 && DrawX<=304 && DrawY>=91 && DrawY<=131))
		  begin	
				Red = 8'h7a;
				Green = 8'hde;
				Blue = 8'h37;
		  end
		  else if ((DrawX>=304 && DrawX<=336 && DrawY>=91 && DrawY<=131))
		  begin	
				if (fill3 >= 1)
					begin
					Red = 8'h00;
					Green = 8'hff;
					Blue = 8'h00;
					end
				else
					begin
					Red = 8'h00;
					Green = 8'h10;
					Blue = 8'h36;
					end
		  end
		  else if ((DrawX>=336 && DrawX<=417 && DrawY>=91 && DrawY<=131))
		  begin	
				Red = 8'h7a;
				Green = 8'hde;
				Blue = 8'h37;
		  end
		  else if ((DrawX>=417 && DrawX<=447 && DrawY>=91 && DrawY<=131))
		  begin	
				if (fill4 >= 1)
					begin
					Red = 8'h00;
					Green = 8'hff;
					Blue = 8'h00;
					end
				else
					begin
					Red = 8'h00;
					Green = 8'h10;
					Blue = 8'h36;
					end
		  end
		  else if ((DrawX>=447 && DrawX<=528 && DrawY>=91 && DrawY<=131))
		  begin	
				Red = 8'h7a;
				Green = 8'hde;
				Blue = 8'h37;
		  end
		  else if ((DrawX>=528 && DrawX<=558 && DrawY>=91 && DrawY<=131))
		  begin	
				if (fill5 >= 1)
					begin
					Red = 8'h00;
					Green = 8'hff;
					Blue = 8'h00;
					end
				else
					begin
					Red = 8'h00;
					Green = 8'h10;
					Blue = 8'h36;
					end
		  end
		  else if ((DrawX>=558 && DrawX<=639 && DrawY>=61 && DrawY<=131))
		  begin	
				Red = 8'h7a;
				Green = 8'hde;
				Blue = 8'h37;
		  end
		  else if ((DrawX>=81 && DrawX<=558 && DrawY>=61 && DrawY<=91))
		  begin	
				Red = 8'h7a;
				Green = 8'hde;
				Blue = 8'h37;
		  end 
		  else if ((car_on == 1'b1))
		  begin
				Red = 8'hff;
				Green = 8'h00;
				Blue = 8'h00;
		  end
		  else if ((car1_on == 1'b1))
		  begin
				Red = 8'hff;
				Green = 8'hff;
				Blue = 8'hff;
		  end
		  else if ((car2_on == 1'b1))
		  begin
				Red = 8'hff;
				Green = 8'h00;
				Blue = 8'h00;
		  end
		  else if ((car3_on == 1'b1))
		  begin
				Red = 8'hff;
				Green = 8'hff;
				Blue = 8'hff;
		  end
		  else if ((car4_on == 1'b1))
		  begin
				Red = 8'h00;
				Green = 8'h00;
				Blue = 8'hff;
		  end
		  else if ((car5_on == 1'b1))
		  begin
				Red = 8'h00;
				Green = 8'h00;
				Blue = 8'hff;
		  end
		  else if ((title_on == 1'b1))
		  begin
				Red = TitleRed;
				Green = TitleGreen;
				Blue = TitleBlue;
				//Red = 8'hff;
				//Green = 8'hff;
				//Blue = 8'hff;
		  end
		  else
		  begin
				Red = 8'h00;
				Green = 8'h00;
            Blue = 8'h00;
		  end
		  end
    end 

assign PadOn = pad_on;
    
endmodule
