module countdown ( input Clk, Reset, output [8:0] seconds);

	parameter clock_frequency = 60;
	logic [31:0] count;
	logic [8:0] SecondCT;

	always @(posedge Reset or negedge Clk)
		begin
		if(Reset)
			begin
			count <= 0;
			SecondCT <= 250;
			end
		else
			begin
			if (count == clock_frequency)
				begin
				SecondCT <= SecondCT - 1;
				count <= 0;
				end
			else if (SecondCT <= 1)
			SecondCT <= 1;
			else
			count <= count + 1;
			end
		end
		
	assign seconds = SecondCT;
	
endmodule
