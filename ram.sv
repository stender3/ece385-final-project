
module  TitleRed
(
		input [18:0] read_address,
		input Clk,

		output logic [4:0] red_out
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:32611];

initial
begin
	 $readmemh("FROGGER_385R.txt", mem);
end


always_ff @ (posedge Clk) begin
	red_out<= mem[read_address];
end

endmodule

module  TitleGreen
(
		input [18:0] read_address,
		input Clk,

		output logic [4:0] green_out
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:32611];

initial
begin
	 $readmemh("FROGGER_385G.txt", mem);
end


always_ff @ (posedge Clk) begin
	green_out<= mem[read_address];
end

endmodule

module  TitleBlue
(
		input [18:0] read_address,
		input Clk,

		output logic [4:0] blue_out
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:32611];

initial
begin
	 $readmemh("FROGGER_385B.txt", mem);
end


always_ff @ (posedge Clk) begin
	blue_out<= mem[read_address];
end

endmodule






























module  TitleCardRed
(
		input [18:0] read_address1,
		input Clk,

		output logic [4:0] red_out1
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:34999];

initial
begin
	 $readmemh("TitleCardR.txt", mem);
end


always_ff @ (posedge Clk) begin
	red_out1<= mem[read_address1];
end

endmodule

module  TitleCardGreen
(
		input [18:0] read_address1,
		input Clk,

		output logic [4:0] green_out1
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:34999];

initial
begin
	 $readmemh("TitleCardG.txt", mem);
end


always_ff @ (posedge Clk) begin
	green_out1<= mem[read_address1];
end

endmodule

module  TitleCardBlue
(
		input [18:0] read_address1,
		input Clk,

		output logic [4:0] blue_out1
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:34999];

initial
begin
	 $readmemh("TitleCardB.txt", mem);
end


always_ff @ (posedge Clk) begin
	blue_out1<= mem[read_address1];
end

endmodule
































module  WinRed
(
		input [18:0] read_address2,
		input Clk,

		output logic [4:0] red_out2
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:89999];

initial
begin
	 $readmemh("WINR.txt", mem);
end


always_ff @ (posedge Clk) begin
	red_out2<= mem[read_address2];
end

endmodule

module  WinGreen
(
		input [18:0] read_address2,
		input Clk,

		output logic [4:0] green_out2
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:89999];

initial
begin
	 $readmemh("WING.txt", mem);
end


always_ff @ (posedge Clk) begin
	green_out2<= mem[read_address2];
end

endmodule

module  WinBlue
(
		input [18:0] read_address2,
		input Clk,

		output logic [4:0] blue_out2
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:89999];

initial
begin
	 $readmemh("WINB.txt", mem);
end


always_ff @ (posedge Clk) begin
	blue_out2<= mem[read_address2];
end

endmodule
































module  LoseRed
(
		input [18:0] read_address2,
		input Clk,

		output logic [4:0] red_out3
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:89999];

initial
begin
	 $readmemh("LOSER.txt", mem);
end


always_ff @ (posedge Clk) begin
	red_out3<= mem[read_address2];
end

endmodule

module  LoseGreen
(
		input [18:0] read_address2,
		input Clk,

		output logic [4:0] green_out3
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:89999];

initial
begin
	 $readmemh("LOSEG.txt", mem);
end


always_ff @ (posedge Clk) begin
	green_out3<= mem[read_address2];
end

endmodule

module  LoseBlue
(
		input [18:0] read_address2,
		input Clk,

		output logic [4:0] blue_out3
);

// mem has width of 4 bits and a total of 156 addresses
logic [9:0] mem [0:89999];

initial
begin
	 $readmemh("LOSEB.txt", mem);
end


always_ff @ (posedge Clk) begin
	blue_out3<= mem[read_address2];
end

endmodule

